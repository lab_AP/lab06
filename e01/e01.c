#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct player{
  char name[30];
  char surname[30];
  int n;
  char **grid;
  struct player* next;
};

typedef struct player Player;

Player *newPlayer(Player **currPlayer, char *name, char *surname, int n, Player ** pFirst);


int main(int argc, char *argv[])
{
  char line[100];
  int i = 0;
  FILE* f1;
  FILE* f2;

  f1 = fopen(argv[1], "r");
  f2 = fopen(argv[3], "w+");
  if (f1 == NULL || f2 == NULL){
    printf("Error opening files");
    exit(1);
  }

  Player *pFirst = NULL;
  while(fgets(line, 100, f1) != NULL){
   int n;
   char name[30];
   char surname[30];
   sscanf(line, "%s%s%d", surname, name, &n);

   Player *currPlayer = newPlayer(&currPlayer, surname, name, n, &pFirst);
   for(i=0; i<n; i++)
     fgets(currPlayer->grid[i], 100, f1);
  }
  
  while(pFirst->next != NULL){
    printf("\n%s%s\n", pFirst->name, pFirst->surname);
    pFirst = pFirst->next;
  }

  return(0);
}

Player *newPlayer(Player **currPlayer, char *name, char *surname, int n, Player ** pFirst)
{
  static int c = 0;
  int i;
  Player *newPlayer = malloc(sizeof(Player));
  if(newPlayer == NULL){
    printf("Fail allocating new player");
    exit(1);
  }
  if(c==0)
    *pFirst = *currPlayer;
  strcpy(newPlayer->name, name);
  strcpy(newPlayer->surname, surname);
  newPlayer->n = n;
  newPlayer->next = NULL;
  newPlayer->grid = (char **) malloc(n * sizeof(char *));
  if(newPlayer->grid == NULL)
    printf("Error allocating grid to newPlayer");

  for(i=0; i<n;i ++)
    newPlayer->grid[i] = (char *) malloc(14 * sizeof(char));

  *currPlayer = newPlayer;

  return(newPlayer); 

}
